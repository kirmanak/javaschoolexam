package com.tsystems.javaschool.tasks.calculator;

/**
 * An enumation of mathematical operators
 */
public enum Operator {
    LeftBracket(false),
    Addiction(false),
    Subtraction(false),
    Divide(true),
    Multiply(true);
    private final boolean hasPriority;

    Operator(final boolean hasPriority) {
        this.hasPriority = hasPriority;
    }

    /**
     * Converts a char to enum
     *
     * @param token a character which could be an operand
     * @return null if token is not an operand, otherwise an enum
     */
    public static Operator fromChar(char token) {
        switch (token) {
            case '(':
                return Operator.LeftBracket;
            case '-':
                return Operator.Subtraction;
            case '+':
                return Operator.Addiction;
            case '/':
                return Operator.Divide;
            case '*':
                return Operator.Multiply;
            default:
                return null;
        }
    }

    /**
     * Returns true if token is supported mathematical operator
     *
     * @param token a character which should be tested
     * @return boolean value containing decision
     */
    public static boolean isOperator(char token) {
        return Operator.fromChar(token) != null;
    }

    public boolean hasPriority() {
        return hasPriority;
    }

    @Override
    public String toString() {
        switch (this) {
            case LeftBracket:
                return "(";
            case Subtraction:
                return "-";
            case Addiction:
                return "+";
            case Divide:
                return "/";
            case Multiply:
                return "*";
            default:
                throw new RuntimeException("You've forgot to override toString for a new operator");
        }
    }
}
