package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Stack;

public class Calculator {
    /**
     * Precision of calculations
     */
    private static final int CALCULATIONS_SCALE = 10;

    /**
     * Precision of the answer
     */
    private static final int ANSWER_SCALE = 4;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) return null;
        String converted = conversion(statement);
        if (converted == null) return null;
        char[] chars = converted.toCharArray();
        Stack<BigDecimal> numbersStack = new Stack<>();
        StringBuilder numbersBuilder = new StringBuilder();
        for (char token : chars) {
            if (Character.isDigit(token) || token == '.') {
                numbersBuilder.append(token);
            } else {
                if (numbersBuilder.length() != 0) {
                    String number = numbersBuilder.toString();
                    BigDecimal value;
                    try {
                        value = new BigDecimal(number);
                    } catch (NumberFormatException err) {
                        return null;
                    }
                    numbersStack.push(value);
                    numbersBuilder = new StringBuilder();
                }
                if (token == ' ') continue;
                Operator operator = Operator.fromChar(token);
                if (numbersStack.size() < 2) return null;
                BigDecimal secondOperand = numbersStack.pop();
                BigDecimal firstOperand = numbersStack.pop();
                switch (operator) {
                    case Divide: {
                        if (secondOperand.equals(BigDecimal.ZERO)) return null;
                        numbersStack.push(firstOperand.divide(secondOperand, CALCULATIONS_SCALE, RoundingMode.HALF_EVEN));
                        break;
                    }
                    case Multiply:
                        numbersStack.push(firstOperand.multiply(secondOperand));
                        break;
                    case Addiction:
                        numbersStack.push(firstOperand.add(secondOperand));
                        break;
                    case Subtraction:
                        numbersStack.push(firstOperand.subtract(secondOperand));
                        break;
                    default:
                        throw new RuntimeException("Something went wrong");
                }
            }
        }
        if (numbersStack.size() != 1) return null;

        BigDecimal answer = numbersStack.pop();
        answer = answer.setScale(ANSWER_SCALE, RoundingMode.HALF_EVEN).stripTrailingZeros();

        return answer.toPlainString();
    }

    /**
     * Converts statement from infix notation to postfix using Shunting-yard algorithm by Dijkstra
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     *                  Becomes: <code>1 38 + 4.5 * 1 2 / -</code>
     * @return string value containing result of conversion or null if statement is invalid
     */
    public String conversion(String statement) {
        StringBuilder answer = new StringBuilder();
        Stack<Operator> operatorStack = new Stack<>();
        char[] charArray = statement.toCharArray();

        for (int i = 0; i < charArray.length; i++) {
            char token = charArray[i];
            if (Character.isDigit(token)) {
                while (true) {
                    answer.append(token);
                    if (i == charArray.length - 1) break;
                    token = charArray[i + 1];
                    if (token != '.' && !Character.isDigit(token)) break;
                    i++;
                }
                answer.append(" ");
            } else if (Operator.isOperator(token)) {
                Operator operator = Operator.fromChar(token);
                if (operator == Operator.LeftBracket) {
                    operatorStack.push(operator);
                    continue;
                }
                // while operator on top of the stack exists and is not a left bracket
                while (!operatorStack.isEmpty() && operatorStack.peek() != Operator.LeftBracket) {
                    Operator topOperator = operatorStack.peek();
                    // while operator's precedence is lower or equals to topOperator's precedence
                    if ((operator.hasPriority() && !topOperator.hasPriority())) break;
                    answer.append(topOperator.toString());
                    answer.append(" ");
                    operatorStack.pop();
                }
                operatorStack.push(operator);
            } else if (token == ')') {
                if (operatorStack.isEmpty()) return null;
                Operator currentOperator = operatorStack.pop();
                while (currentOperator != Operator.LeftBracket) {
                    if (operatorStack.isEmpty()) return null;
                    answer.append(currentOperator.toString());
                    answer.append(" ");
                    currentOperator = operatorStack.pop();
                }
            } else if (token != ' ') {
                return null;
            }
        }

        while (!operatorStack.isEmpty()) {
            Operator operator = operatorStack.pop();
            if (operator == Operator.LeftBracket) {
                if (operatorStack.isEmpty()) return null;
                continue;
            }
            answer.append(operator.toString());
            answer.append(" ");
        }

        return answer.toString().trim();
    }
}
