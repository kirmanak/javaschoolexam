package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     *
     * @throws IllegalArgumentException if either of the links is null.
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException("X or Y is null");

        Iterator xIterator = x.iterator();
        Object currentX;

        if (xIterator.hasNext())
            currentX = xIterator.next();
        else
            return true;

        for (Object o : y) {
            if (o.equals(currentX)) {
                if (xIterator.hasNext()) currentX = xIterator.next();
                else return true;
            }
        }

        return false;
    }
}
