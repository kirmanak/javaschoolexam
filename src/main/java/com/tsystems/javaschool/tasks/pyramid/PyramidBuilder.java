package com.tsystems.javaschool.tasks.pyramid;

import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null) throw new CannotBuildPyramidException();

        // calculates how many levels the pyramid has
        double levelsCounter = (Math.sqrt(inputNumbers.size() * 8 + 1) - 1);
        if (levelsCounter % 2 != 0) throw new CannotBuildPyramidException();
        final int ROWS = (int) levelsCounter / 2;
        final int COLUMNS = ROWS + (ROWS - 1);

        // sorts the array and checks it for null
        try {
            inputNumbers.sort(null);
        } catch (NullPointerException err) {
            throw new CannotBuildPyramidException();
        }

        int result[][] = new int[ROWS][COLUMNS];
        Iterator<Integer> iterator = inputNumbers.iterator();

        for (int row = 0; row < ROWS; row++) {
            final int trailingZeros = ROWS - row - 1;
            int column = 0;
            for (; column < trailingZeros; column++) {
                result[row][column] = 0;
            }
            for (; column < (COLUMNS - trailingZeros); column++) {
                int number = iterator.next();
                result[row][column] = number;
                column++;
                if (column < COLUMNS - 1) result[row][column] = 0;
            }
            for (; column < COLUMNS; column++) {
                result[row][column] = 0;
            }
        }

        return result;
    }
}
